package service

import (
	"errors"
	"fmt"
)

// On déclare le nom de notre nouveau service
const MyServiceName Name = "myService"

// On déclare le type de notre nouveau service
type MyService struct{}

// Une simple méthode exposée sur notre service
func (s *MyService) HelloWorld() {
	fmt.Println("Hello World !")
}

func Example_usage() {

	// On initialise un conteneur de services vide
	container := NewContainer()

	// On créait un "fournisseur" pour notre nouveau service.
	// Celui ci sera utilisé pour créer une nouvelle instance du service
	// à chaque fois qu'un appel à container.Service(MyServiceName)
	// sera fait
	myServiceProvider := func(container *Container) (interface{}, error) {
		// On peut potentiellement appeler d'autres services ici
		// via le conteneur afin d'initialiser notre propre service
		return &MyService{}, nil
	}

	// On expose le fournisseur de service dans notre conteneur de service
	// Cette opération est typiquement faite lors de l'initialisation de
	// l'application, avant le lancement du serveur HTTP
	container.Provide(MyServiceName, myServiceProvider)

	// Plus tard, on peut utiliser le conteneur pour récupérer une instance
	// de notre service
	service, err := container.Service(MyServiceName)
	if err != nil {
		panic(err)
	}

	// Le service est retourné en tant que interface{}, il faut utiliser
	// de l'assertion de type afin de pouvoir l'utiliser concrètement.

	// Les packages de services proposés dans goweb exposent tous les méthodes
	// From(container *service.Container) (Service, error)
	// et Must(container *service.Container) Service qui permettent de simplifier
	// ce processus.

	myService, ok := service.(*MyService)
	if !ok {
		panic(errors.New("unexpected implementation"))
	}

	// On peut maintenant utiliser le service
	myService.HelloWorld()

}
