package template

import (
	"io"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

// ServiceName defines the Tempate service name
const ServiceName service.Name = "template"

// Service is a templating service
type Service interface {
	RenderPage(w http.ResponseWriter, templateName string, data interface{}) error
	Render(w io.Writer, templateName string, data interface{}) error
}

// From retrieves the template service in the given container
func From(container *service.Container) (Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}
	templateService, ok := service.(Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}
	return templateService, nil
}

// Must retrieves the template service in the given container or panic otherwise
func Must(container *service.Container) Service {
	service, err := From(container)
	if err != nil {
		panic(err)
	}
	return service
}
