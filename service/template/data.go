package template

import (
	"net/http"

	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/build"
)

// Data is some data to inject into the template
type Data map[string]interface{}

// DataExtFunc is some extensions to a template's data
type DataExtFunc func(data Data) (Data, error)

// Extend returns a template's data with the given extensions
func Extend(data Data, extensions ...DataExtFunc) (Data, error) {
	var err error
	for _, ext := range extensions {
		data, err = ext(data)
		if err != nil {
			return nil, err
		}
	}
	return data, nil
}

func WithBuildInfo(w http.ResponseWriter, r *http.Request, ctn *service.Container) DataExtFunc {
	return func(data Data) (Data, error) {
		info, err := build.From(ctn)
		if err != nil {
			return nil, err
		}
		data["BuildInfo"] = info
		return data, nil
	}
}
