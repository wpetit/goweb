package build

import (
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "build"
)

func ServiceProvider(projectVersion, gitRef, buildDate string) service.Provider {
	info := &Info{projectVersion, gitRef, buildDate}

	return func(ctn *service.Container) (interface{}, error) {
		return info, nil
	}
}

// From retrieves the build informations in the given service container
func From(container *service.Container) (*Info, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Info)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the user repository in the given service container or panic otherwise
func Must(container *service.Container) *Info {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
