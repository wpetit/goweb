package session

const (
	// FlashError defines an "error" flash message
	FlashError FlashType = "error"
	// FlashWarn defines an "warning" flash message
	FlashWarn FlashType = "warn"
	// FlashSuccess defines an "success" flash message
	FlashSuccess FlashType = "success"
	// FlashInfo defines an "info" flash message
	FlashInfo FlashType = "info"
)

// FlashType defines the type of a flash message
type FlashType string

// Flash is a ephemeral message that lives in a session
// until it's read
type Flash interface {
	Type() FlashType
	Message() string
}

// BaseFlash is a base implementation of a flash message
type BaseFlash struct {
	flashType FlashType
	message   string
}

// Type returns the type of the flash
func (f *BaseFlash) Type() FlashType {
	return f.flashType
}

// Message returns the message of the flash
func (f *BaseFlash) Message() string {
	return f.message
}

// NewBaseFlash returns a new BaseFlash
func NewBaseFlash(flashType FlashType, message string) *BaseFlash {
	return &BaseFlash{flashType, message}
}
