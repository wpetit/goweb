package session

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

// ServiceName defines the Session service name
const ServiceName service.Name = "session"

// Service defines the API of a "http session" service
type Service interface {
	Get(http.ResponseWriter, *http.Request) (Session, error)
}

// From retrieves the session service in the given container
func From(container *service.Container) (Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}
	sessionService, ok := service.(Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}
	return sessionService, nil
}

// Must retrieves the session service in the given container or panic otherwise
func Must(container *service.Container) Service {
	service, err := From(container)
	if err != nil {
		panic(err)
	}
	return service
}
