package session

import (
	"net/http"
)

// Session defines the API of a Session
type Session interface {
	Set(string, interface{})
	Unset(string)
	Get(string) interface{}
	AddFlash(flashType FlashType, message string)
	Flashes(flashTypes ...FlashType) []Flash
	Save(http.ResponseWriter, *http.Request) error
	Delete(http.ResponseWriter, *http.Request) error
}
