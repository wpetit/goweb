package logger

import (
	"context"
	"io"
	"os"

	"github.com/pkg/errors"

	"cdr.dev/slog/sloggers/slogjson"

	"cdr.dev/slog/sloggers/sloghuman"

	"cdr.dev/slog"
)

type Format string

const (
	FormatHuman Format = "human"
	FormatJSON  Format = "json"
)

var captureFunc CaptureFunc = func(err error) {}

var defaultLogger slog.Logger // nolint: gochecknoglobals

type Field = slog.Field
type Map = slog.Map
type Level = slog.Level

const (
	LevelCritical Level = slog.LevelCritical
	LevelError    Level = slog.LevelError
	LevelWarn     Level = slog.LevelWarn
	LevelInfo     Level = slog.LevelInfo
	LevelDebug    Level = slog.LevelDebug
)

func init() { // nolint: gochecknoinits
	defaultLogger = Make(FormatHuman, os.Stdout)
}

func Make(f Format, w io.Writer) slog.Logger {
	var sink slog.Sink

	switch f {
	case FormatHuman:
		sink = sloghuman.Sink(w)
	case FormatJSON:
		sink = slogjson.Sink(w)
	default:
		panic(errors.Errorf("unknown logger format '%s'", f))
	}

	return slog.Make(sink)
}

func Debug(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Debug(ctx, msg, fields...)
}

func Info(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Info(ctx, msg, fields...)
}

func Warn(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Warn(ctx, msg, fields...)
}

func Error(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Error(ctx, msg, fields...)
}

func Critical(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Critical(ctx, msg, fields...)
}

func Fatal(ctx context.Context, msg string, fields ...any) {
	slog.Helper()
	defaultLogger.Fatal(ctx, msg, fields...)
}

func F(name string, value interface{}) Field {
	return slog.F(name, value)
}

func M(fields ...Field) Map {
	return slog.M(fields...)
}

func E(err error) Field {
	return slog.Error(err)
}

func CapturedE(err error) Field {
	captureFunc(err)
	return slog.Error(err)
}

func SetLevel(level Level) {
	defaultLogger = defaultLogger.Leveled(level)
}

func SetFormat(format Format) {
	defaultLogger = Make(format, os.Stdout)
}

func With(ctx context.Context, fields ...Field) context.Context {
	return slog.With(ctx, fields...)
}

type CaptureFunc func(err error)

func SetCaptureFunc(fn CaptureFunc) {
	captureFunc = fn
}
