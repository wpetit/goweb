package logger

import (
	"context"
	"os"
	"testing"
)

func TestLogger(t *testing.T) {
	log := Make(FormatHuman, os.Stdout)

	log.Info(context.Background(), "test")
}
