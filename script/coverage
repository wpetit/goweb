#!/bin/bash

set -eo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
GO_PACKAGE_DIRS=$(find . -not -path './vendor/*' -name '*.go' -printf '%h\n' | uniq)
COVERAGE_DIR="$DIR/../coverage"

rm -rf "$COVERAGE_DIR"
mkdir -p "$COVERAGE_DIR"

for dir in $GO_PACKAGE_DIRS; do
  pushd $dir > /dev/null
  go test -coverprofile="$DIR/../coverage/${dir//\//_}.out" > /dev/null
  popd > /dev/null
done

OUTPUT_FILES=$(find "$COVERAGE_DIR" -name '*.out')
for out in $OUTPUT_FILES; do
  package=$(realpath --relative-to="$DIR/../coverage" "${out}" | sed -e 's/\.out$//' -e 's/^\._//')
  echo "-- Package '${package//_/\/}' --"
  echo
  go tool cover -func="$out"
  echo
done