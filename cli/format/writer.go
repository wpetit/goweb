package format

import "io"

type OutputMode string

const (
	OutputModeWide    OutputMode = "wide"
	OutputModeCompact OutputMode = "compact"
)

type Hints struct {
	Props      []Prop
	OutputMode OutputMode
}

type Writer interface {
	Write(writer io.Writer, hints Hints, data ...any) error
}
