package format

type PropHintName string

type PropHintFunc func() (PropHintName, any)

type Prop struct {
	name  string
	label string
	hints map[PropHintName]any
}

func (p *Prop) Name() string {
	return p.name
}

func (p *Prop) Label() string {
	return p.label
}

func NewProp(name, label string, funcs ...PropHintFunc) Prop {
	hints := make(map[PropHintName]any)
	for _, fn := range funcs {
		name, value := fn()
		hints[name] = value
	}

	return Prop{name, label, hints}
}

func WithPropHint(name PropHintName, value any) PropHintFunc {
	return func() (PropHintName, any) {
		return name, value
	}
}

func PropHint[T any](p Prop, name PropHintName, defaultValue T) T {
	rawValue, exists := p.hints[name]
	if !exists {
		return defaultValue
	}

	value, ok := rawValue.(T)
	if !ok {
		return defaultValue
	}

	return value
}
