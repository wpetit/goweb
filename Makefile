test:
	go clean -testcache
	go test -v -race ./...

watch:
	modd
	
deps:
	go get -u golang.org/x/tools/cmd/godoc
	go get -u github.com/cortesi/modd/cmd/modd
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

coverage:
	@script/coverage

lint:
	golangci-lint run --tests=false --enable-all

clean:
	rm -rf ./bin ./release ./coverage

doc:
	@echo "open your browser to http://localhost:6060/pkg/gitlab.com/wpetit/goweb to see the documentation"
	godoc -http=:6060


.PHONY: test clean lint coverage doc