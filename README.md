# goweb

Librairie d'aide à la création d'applications web pour Go

**Librairie en cours de développement.** Des changements d'API peuvent intervenir à tout moment.

## Documentation

https://godoc.org/gitlab.com/wpetit/goweb

## License

AGPL-3.0