package cqrs

import "errors"

var (
	ErrHandlerNotFound   = errors.New("handler not found")
	ErrUnexpectedRequest = errors.New("unexpected request")
	ErrUnexpectedData    = errors.New("unexpected data")
)
