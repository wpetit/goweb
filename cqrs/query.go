package cqrs

import (
	"context"

	"github.com/google/uuid"
)

type QueryID string

type QueryRequest interface{}

type Query interface {
	ID() QueryID
	Request() QueryRequest
}

type BaseQuery struct {
	id  QueryID
	req QueryRequest
}

func (q *BaseQuery) ID() QueryID {
	return q.id
}

func (q *BaseQuery) Request() QueryRequest {
	return q.req
}

func NewBaseQuery(req QueryRequest) *BaseQuery {
	id := QueryID(uuid.New().String())
	return &BaseQuery{id, req}
}

type QueryResult interface {
	Query() Query
	Data() interface{}
}

type BaseQueryResult struct {
	qry  Query
	data interface{}
}

func (r *BaseQueryResult) Query() Query {
	return r.qry
}

func (r *BaseQueryResult) Data() interface{} {
	return r.data
}

func NewBaseQueryResult(qry Query, data interface{}) *BaseQueryResult {
	return &BaseQueryResult{qry, data}
}

type QueryHandlerFunc func(context.Context, Query) (interface{}, error)

func (h QueryHandlerFunc) Handle(ctx context.Context, qry Query) (interface{}, error) {
	return h(ctx, qry)
}

type QueryHandler interface {
	Handle(context.Context, Query) (interface{}, error)
}
