package cqrs

import "testing"

type testType struct{}

func TestMatchCommandRequestType(t *testing.T) {
	match := MatchCommandRequest(&testType{})

	cmd := NewBaseCommand(&testType{})

	matches, err := match(cmd)
	if err != nil {
		t.Error(err)
	}

	if e, g := true, matches; e != g {
		t.Errorf("match(&testType{}): expected '%v', got '%v'", e, g)
	}

	cmd = NewBaseCommand(nil)

	matches, err = match(cmd)
	if err != nil {
		t.Error(err)
	}

	if e, g := false, matches; e != g {
		t.Errorf("match(nil): expected '%v', got '%v'", e, g)
	}

	cmd = NewBaseCommand("test")

	matches, err = match(cmd)
	if err != nil {
		t.Error(err)
	}

	if e, g := false, matches; e != g {
		t.Errorf("match(\"test\"): expected '%v', got '%v'", e, g)
	}
}
