package cqrs

import "gitlab.com/wpetit/goweb/service"

// ServiceProvider returns a service.Provider for the
// the cqrs service
func ServiceProvider() service.Provider {
	dispatcher := NewDispatcher()

	return func(container *service.Container) (interface{}, error) {
		return dispatcher, nil
	}
}
