package cqrs

import (
	"context"
	"testing"
)

func TestBasicCommandMiddleware(t *testing.T) {
	dispatcher := NewDispatcher()

	handlerCalled := false

	handleTestCommand := func(ctx context.Context, cmd Command) error {
		handlerCalled = true
		return nil
	}

	middlewareCalled := false

	commandMiddleware := func(next CommandHandler) CommandHandler {
		fn := func(ctx context.Context, cmd Command) error {
			middlewareCalled = true
			return next.Handle(ctx, cmd)
		}

		return CommandHandlerFunc(fn)
	}

	dispatcher.RegisterCommand(
		MatchCommandRequest(&testCommandRequest{}),
		CommandHandlerFunc(handleTestCommand),
		commandMiddleware,
	)

	cmd := &testCommandRequest{
		Foo: "bar",
	}
	ctx := context.Background()

	result, err := dispatcher.Exec(ctx, cmd)
	if err != nil {
		t.Error(err)
	}

	if result == nil {
		t.Error("result should not be nil")
	}

	if result.Command() == nil {
		t.Error("result.Command() should not be nil")
	}

	if e, g := result.Command().Request(), cmd; e != g {
		t.Errorf("result.Command().Request(): expected '%v', got '%v'", e, g)
	}

	if e, g := middlewareCalled, true; e != g {
		t.Errorf("middlewareCalled: expected '%v', got '%v'", e, g)
	}

	if e, g := handlerCalled, true; e != g {
		t.Errorf("handlerCalled: expected '%v', got '%v'", e, g)
	}
}
