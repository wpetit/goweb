package cqrs

import (
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

// ServiceName defines the cqrs service name
const ServiceName service.Name = "cqrs"

// From retrieves the cqrs service in the given container
func From(container *service.Container) (*Dispatcher, error) {
	raw, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := raw.(*Dispatcher)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the cqrs service in the given container or panic otherwise
func Must(container *service.Container) *Dispatcher {
	service, err := From(container)
	if err != nil {
		panic(err)
	}

	return service
}
