package cqrs

type Option struct {
	CommandFactory       CommandFactory
	QueryFactory         QueryFactory
	CommandResultFactory CommandResultFactory
	QueryResultFactory   QueryResultFactory
}

type CommandFactory func(CommandRequest) (Command, error)
type QueryFactory func(QueryRequest) (Query, error)
type CommandResultFactory func(Command) (CommandResult, error)
type QueryResultFactory func(Query, interface{}) (QueryResult, error)

type OptionFunc func(*Option)

func DefaultOption() *Option {
	funcs := []OptionFunc{
		WithBaseQueryFactory(),
		WithBaseCommandFactory(),
		WithBaseQueryResultFactory(),
		WithBaseCommandResultFactory(),
	}

	opt := &Option{}

	for _, fn := range funcs {
		fn(opt)
	}

	return opt
}

func CreateOption(funcs ...OptionFunc) *Option {
	opt := DefaultOption()

	for _, fn := range funcs {
		fn(opt)
	}

	return opt
}

func WithCommandFactory(factory CommandFactory) OptionFunc {
	return func(opt *Option) {
		opt.CommandFactory = factory
	}
}

func WithQueryFactory(factory QueryFactory) OptionFunc {
	return func(opt *Option) {
		opt.QueryFactory = factory
	}
}

func WithBaseQueryFactory() OptionFunc {
	return func(opt *Option) {
		opt.QueryFactory = func(req QueryRequest) (Query, error) {
			return NewBaseQuery(req), nil
		}
	}
}

func WithBaseCommandFactory() OptionFunc {
	return func(opt *Option) {
		opt.CommandFactory = func(req CommandRequest) (Command, error) {
			return NewBaseCommand(req), nil
		}
	}
}

func WithBaseQueryResultFactory() OptionFunc {
	return func(opt *Option) {
		opt.QueryResultFactory = func(qry Query, data interface{}) (QueryResult, error) {
			return NewBaseQueryResult(qry, data), nil
		}
	}
}

func WithBaseCommandResultFactory() OptionFunc {
	return func(opt *Option) {
		opt.CommandResultFactory = func(cmd Command) (CommandResult, error) {
			return NewBaseCommandResult(cmd), nil
		}
	}
}
