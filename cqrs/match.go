package cqrs

import (
	"reflect"
)

type MatchFunc func(interface{}) (bool, error)

func MatchCommandRequest(req interface{}) MatchFunc {
	reqType := reflect.TypeOf(req)

	return func(raw interface{}) (bool, error) {
		cmd, ok := raw.(Command)
		if !ok {
			return false, ErrUnexpectedRequest
		}

		return reflect.TypeOf(cmd.Request()) == reqType, nil
	}
}

func MatchQueryRequest(req interface{}) MatchFunc {
	reqType := reflect.TypeOf(req)

	return func(raw interface{}) (bool, error) {
		cmd, ok := raw.(Query)
		if !ok {
			return false, ErrUnexpectedRequest
		}

		return reflect.TypeOf(cmd.Request()) == reqType, nil
	}
}
