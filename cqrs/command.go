package cqrs

import (
	"context"

	"github.com/google/uuid"
)

type CommandID string

type CommandRequest interface {
}

type Command interface {
	ID() CommandID
	Request() CommandRequest
}

type BaseCommand struct {
	id  CommandID
	req CommandRequest
}

func (c *BaseCommand) ID() CommandID {
	return c.id
}

func (c *BaseCommand) Request() CommandRequest {
	return c.req
}

func NewBaseCommand(req CommandRequest) *BaseCommand {
	id := CommandID(uuid.New().String())
	return &BaseCommand{id, req}
}

type CommandResult interface {
	Command() Command
}

type BaseCommandResult struct {
	cmd Command
}

func (r *BaseCommandResult) Command() Command {
	return r.cmd
}

func NewBaseCommandResult(cmd Command) *BaseCommandResult {
	return &BaseCommandResult{cmd}
}

type CommandHandlerFunc func(context.Context, Command) error

func (h CommandHandlerFunc) Handle(ctx context.Context, cmd Command) error {
	return h(ctx, cmd)
}

type CommandHandler interface {
	Handle(context.Context, Command) error
}
