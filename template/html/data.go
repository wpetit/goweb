package html

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

// WithFlashes extends the template's data with session's flash messages
func WithFlashes(w http.ResponseWriter, r *http.Request, container *service.Container) template.DataExtFunc {
	return func(data template.Data) (template.Data, error) {

		sessionService, err := session.From(container)
		if err != nil {
			return nil, errors.Wrap(err, "error while retrieving session service")
		}

		sess, err := sessionService.Get(w, r)
		if err != nil {
			return nil, errors.Wrap(err, "error while retrieving session")
		}

		flashes := sess.Flashes(
			session.FlashError, session.FlashWarn,
			session.FlashSuccess, session.FlashInfo,
		)

		data["Flashes"] = flashes

		if err := sess.Save(w, r); err != nil {
			return nil, errors.Wrap(err, "error while saving session")
		}

		return data, nil

	}
}
