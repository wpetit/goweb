package html

import "gitlab.com/wpetit/goweb/service"

// ServiceProvider returns a service.Provider for the
// the HTML template service implementation
func ServiceProvider(loader Loader, funcs ...OptionFunc) service.Provider {
	templateService := NewTemplateService(loader, funcs...)
	err := templateService.Load()
	return func(container *service.Container) (interface{}, error) {
		if err != nil {
			return nil, err
		}
		return templateService, nil
	}
}
