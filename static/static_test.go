package static

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestStaticDir(t *testing.T) {

	req, err := http.NewRequest("GET", "/test.txt", nil)
	if err != nil {
		t.Fatal(err)
	}

	notFound := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Error("notFound should not be invoked")
	})

	rr := httptest.NewRecorder()
	handler := Dir("./testdata", "/", notFound)

	handler.ServeHTTP(rr, req)

	if g, e := rr.Code, http.StatusOK; g != e {
		t.Errorf("rr.Code: got '%v', expected '%v'", g, e)
	}

	if g, e := rr.Body.String(), "Hello World"; g != e {
		t.Errorf("rr.Body.String(): got '%v', expected '%v'", g, e)
	}

}
