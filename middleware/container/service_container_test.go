package container

import (
	"context"
	"testing"

	"gitlab.com/wpetit/goweb/service"
)

func TestContextServiceContainer(t *testing.T) {

	container := service.NewContainer()
	ctx := context.WithValue(context.Background(), KeyServiceContainer, container)

	ctn, err := From(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if ctn == nil {
		t.Fatal("container should not be nil")
	}

}

func TestContextInvalidServiceContainer(t *testing.T) {

	invalidContainer := struct{}{}
	ctx := context.WithValue(context.Background(), KeyServiceContainer, invalidContainer)

	container, err := From(ctx)

	if g, e := err, ErrInvalidServiceContainer; g != e {
		t.Errorf("err: got '%v', expected '%v'", g, e)
	}

	if container != nil {
		t.Errorf("container: got '%v', expected '%v'", container, nil)
	}

}
