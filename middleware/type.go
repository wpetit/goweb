package middleware

import "net/http"

// ContextKey are values exposed on the request context
type ContextKey string

// Middleware An HTTP middleware
type Middleware func(http.Handler) http.Handler
