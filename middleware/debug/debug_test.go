package debug

import (
	"context"
	"testing"
)

func TestContextDebug(t *testing.T) {

	debug := false
	ctx := context.WithValue(context.Background(), KeyDebug, debug)

	dbg, err := From(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if dbg {
		t.Fatal("debug should be false")
	}

}
