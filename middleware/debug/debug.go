package debug

import (
	"context"
	"errors"

	"github.com/go-chi/chi/v5/middleware"
	goweb "gitlab.com/wpetit/goweb/middleware"
)

const (
	// KeyDebug is the context key associated with the debug value
	KeyDebug goweb.ContextKey = "debug"
)

// ErrInvalidDebug is returned when no debug value
// could be found on the given context
var ErrInvalidDebug = errors.New("invalid debug")

// From retrieves the debug value from the given context
func From(ctx context.Context) (bool, error) {
	debug, ok := ctx.Value(KeyDebug).(bool)
	if !ok {
		return false, ErrInvalidDebug
	}
	return debug, nil
}

// Must retrieves the debug value from the given context or panics otherwise
func Must(ctx context.Context) bool {
	debug, err := From(ctx)
	if err != nil {
		panic(err)
	}
	return debug
}

// Debug expose the given debug flag as a context value
// on the HTTP requests
func Debug(debug bool) goweb.Middleware {
	return middleware.WithValue(KeyDebug, debug)
}
