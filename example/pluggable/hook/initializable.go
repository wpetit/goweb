package hook

type Initializable interface {
	HookInit() error
}
