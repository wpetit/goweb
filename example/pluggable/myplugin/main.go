package main

import (
	"context"

	"gitlab.com/wpetit/goweb/plugin"
)

func RegisterPlugin(ctx context.Context) (plugin.Plugin, error) {
	return &MyPlugin{}, nil
}
