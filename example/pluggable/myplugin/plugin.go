package main

import "log"

type MyPlugin struct{}

func (e *MyPlugin) PluginName() string {
	return "my.plugin"
}

func (e *MyPlugin) PluginVersion() string {
	return "0.0.0"
}

func (e *MyPlugin) HookInit() error {
	log.Println("MyPlugin initialized !")

	return nil
}
