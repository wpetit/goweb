package main

import (
	"context"
	"log"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/example/pluggable/hook"
	"gitlab.com/wpetit/goweb/plugin"
)

func main() {
	reg := plugin.NewRegistry()
	ctx := context.Background()

	_, err := reg.LoadAll(ctx, "./bin/*.so")
	if err != nil {
		log.Fatal(errors.WithStack(err))
	}

	for _, ext := range reg.Plugins() {
		log.Printf("Loaded plugin '%s', version '%s'", ext.PluginName(), ext.PluginVersion())
	}

	// Iterate over plugins
	err = reg.Each(func(p plugin.Plugin) error {
		h, ok := p.(hook.Initializable)
		if !ok {
			// Skip non initializable plugins
			return nil
		}

		// Initialize plugin
		if err := h.HookInit(); err != nil {
			return errors.WithStack(err)
		}

		return nil
	})

	if err != nil {
		log.Fatal(errors.WithStack(err))
	}
}
