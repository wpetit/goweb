package gorilla

import (
	"github.com/gorilla/sessions"
	"gitlab.com/wpetit/goweb/service"
)

// ServiceProvider returns a service.Provider for the
// the gorilla session service implementation
func ServiceProvider(sessionName string, store sessions.Store) service.Provider {
	sessionService := NewSessionService(sessionName, store)
	return func(container *service.Container) (interface{}, error) {
		return sessionService, nil
	}
}
