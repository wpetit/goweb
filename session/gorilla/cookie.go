package gorilla

import (
	"crypto/rand"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
)

// CreateCookieSessionStore creates and returns a new cookie session store
// with random authentication and encryption keys
func CreateCookieSessionStore(authKeySize, encryptKeySize int, defaultOptions *sessions.Options) (sessions.Store, error) {
	authenticationKey, err := GenerateRandomBytes(authKeySize)
	if err != nil {
		return nil, errors.Wrap(err, "error while generating cookie authentication key")
	}

	encryptionKey, err := GenerateRandomBytes(encryptKeySize)
	if err != nil {
		return nil, errors.Wrap(err, "error while generating cookie encryption key")
	}

	store := sessions.NewCookieStore(authenticationKey, encryptionKey)
	store.Options = defaultOptions

	return store, nil
}

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
