package gorilla

import (
	"net/http"

	"github.com/gorilla/securecookie"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service/session"
)

// SessionService is an implementation of service.Session
// based on the github.com/gorilla/sessions
type SessionService struct {
	sessionName string
	store       sessions.Store
}

// Get returns a Session associated with the given HTTP request
func (s *SessionService) Get(w http.ResponseWriter, r *http.Request) (session.Session, error) {
	sess, err := s.store.Get(r, s.sessionName)
	if err != nil {
		multiErr, ok := err.(securecookie.MultiError)
		if !ok || multiErr.Error() != securecookie.ErrMacInvalid.Error() {
			return nil, errors.Wrap(err, "error while retrieving the session from the request")
		}
	}
	return NewSession(sess), nil
}

// NewSessionService returns a new SessionService backed
// by the given Store
func NewSessionService(sessionName string, store sessions.Store) *SessionService {
	return &SessionService{
		sessionName: sessionName,
		store:       store,
	}
}
