package plugin_test

import (
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/plugin"
)

type testPlugin struct {
	name    string
	version string
}

func (p *testPlugin) PluginName() string {
	return p.name
}

func (p *testPlugin) PluginVersion() string {
	return p.version
}

func (p *testPlugin) Foo() string {
	return "bar"
}

func TestRegistryEach(t *testing.T) {
	t.Parallel()

	reg := plugin.NewRegistry()

	plugins := []*testPlugin{
		{"plugin.a", "0.0.0"},
		{"plugin.b", "0.0.1"},
	}

	for _, p := range plugins {
		reg.Add(p)
	}

	total := 0

	err := reg.Each(func(p plugin.Plugin) error {
		total++

		return nil
	})
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	if e, g := len(plugins), total; e != g {
		t.Errorf("total: expected '%v', got '%v'", e, g)
	}
}

func TestRegistryGet(t *testing.T) {
	t.Parallel()

	reg := plugin.NewRegistry()

	plugins := []*testPlugin{
		{"plugin.a", "0.0.0"},
		{"plugin.b", "0.0.1"},
	}

	for _, p := range plugins {
		reg.Add(p)
	}

	for _, p := range plugins {
		plugin, err := reg.Get(p.name)
		if err != nil {
			t.Error(errors.WithStack(err))
		}

		if e, g := p.name, plugin.PluginName(); e != g {
			t.Errorf("plugin.PluginName(): expected '%v', got '%v'", e, g)
		}

		if e, g := p.version, plugin.PluginVersion(); e != g {
			t.Errorf("plugin.PluginVersion(): expected '%v', got '%v'", e, g)
		}
	}

	p, err := reg.Get("plugin.c")
	if !errors.Is(err, plugin.ErrPluginNotFound) {
		t.Errorf("err: expected '%v', got '%v'", plugin.ErrPluginNotFound, err)
	}

	if p != nil {
		t.Errorf("reg.Get(\"plugin.c\"): expected '%v', got '%v'", nil, p)
	}
}
