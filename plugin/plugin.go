package plugin

type Plugin interface {
	PluginName() string
	PluginVersion() string
}
