package plugin

import "errors"

var (
	// ErrInvalidRegisterFunc is returned when the plugin package
	// could not find the expected RegisterPlugin func in the loaded
	// plugin.
	ErrInvalidRegisterFunc = errors.New("invalid register func")
	// ErrInvalidPlugin is returned when a loaded plugin does
	// not match the expected interface.
	ErrInvalidPlugin = errors.New("invalid plugin")
	// ErrPluginNotFound is returned when the given plugin could
	// not be found in the registry.
	ErrPluginNotFound = errors.New("plugin not found")
)
