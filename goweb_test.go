package goweb

import (
	"net/http"

	"github.com/go-chi/chi"
	containerMiddleware "gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/template"
	"gitlab.com/wpetit/goweb/template/html"
)

func Example_usage() {

	// Exemple d'utilisation de la librairie "goweb" pour la mise en place
	// d'une application web avec injection de dépendances - ici un moteur de templates.

	// On créé un conteneur de service vide
	container := service.NewContainer()

	// On expose le service "template" avec l'implémentation
	// basée sur le moteur de rendu HTML de la librairie standard
	container.Provide(template.ServiceName, html.ServiceProvider(
		html.NewDirectoryLoader("./templates"),
	))

	router := chi.NewRouter()

	// On utilise le middleware "ServiceContainer" pour exposer
	// le conteneur de service dans nos requêtes
	router.Use(containerMiddleware.ServiceContainer(container))

	// On créé un handler pour la page d'accueil
	router.Get("/", ServeHomePage)

	if err := http.ListenAndServe(":3000", router); err != nil {
		panic(err)
	}

}

// ServeHomePage - Sert la page d'accueil de l'application
func ServeHomePage(w http.ResponseWriter, r *http.Request) {

	// On récupère le conteneur de services depuis le contexte de la requête
	container := containerMiddleware.Must(r.Context())

	// On récupère le service "template" depuis le conteneur de service
	templateService := template.Must(container)

	// On utilise notre service de template
	if err := templateService.RenderPage(w, "my-template.html.tmpl", nil); err != nil {
		panic(err)
	}
}
